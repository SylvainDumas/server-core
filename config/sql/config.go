package sql

import (
	"crypto/tls"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	cfg "gitlab.com/SylvainDumas/server-core/config"

	"github.com/go-sql-driver/mysql"
)

const (
	maxOpenIdleConnections = 25
	maxLifeTimeConnections = 5 * time.Minute
)

type SqlConfig struct {
	Db       string                 `json:"db"`
	Host     string                 `json:"host"`
	Port     int                    `json:"port"`
	User     string                 `json:"user"`
	Password string                 `json:"password"`
	Database string                 `json:"database"`
	Options  map[string]interface{} `json:"options"`
	TLS      *cfg.TLSPEM            `json:"tls"`
}

func (obj SqlConfig) getTLSConfig() (*tls.Config, error) {
	if obj.TLS == nil {
		return nil, nil
	}

	return obj.TLS.GetTLSConfig()
}

func (obj SqlConfig) GetDbClient() (*sql.DB, error) {
	tlsConfig, err := obj.getTLSConfig()
	if err != nil {
		return nil, err
	}

	// Datasource name connect builder
	var connect string
	switch obj.Db {
	case "postgres":
		connect = fmt.Sprintf("host=%v port=%v user=%v password=%v dbname=%v",
			obj.Host, obj.Port, obj.User, obj.Password, obj.Database)
		for k, v := range obj.Options {
			connect += fmt.Sprintf(" %v=%v", k, v)
		}
	case "mysql":
		var options []string
		if tlsConfig != nil {
			mysql.RegisterTLSConfig("custom", tlsConfig)
			options = append(options, "tls=custom")
		}

		for k, v := range obj.Options {
			if (tlsConfig != nil && k == "tls") == false {
				options = append(options, fmt.Sprintf("%v=%v", k, v))
			}
		}

		connect = fmt.Sprintf("%v:%v@tcp(%v:%v)/%v", obj.User, obj.Password, obj.Host, obj.Port, obj.Database)
		if len(options) != 0 {
			connect += "?" + strings.Join(options, "&")
		}
	default:
		return nil, errors.New("Model: database connection not supported")
	}

	// Client connect
	client, err := sql.Open(obj.Db, connect)
	if err != nil {
		return nil, err
	}
	// Client connect check
	if err = client.Ping(); err != nil {
		return nil, err
	}

	// Client connections limitations
	client.SetMaxOpenConns(maxOpenIdleConnections)
	client.SetMaxIdleConns(maxOpenIdleConnections)
	client.SetConnMaxLifetime(maxLifeTimeConnections)

	return client, nil
}
