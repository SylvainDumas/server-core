package cfg

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
)

type TLSPEM struct {
	ClientCert string `json:"clientCert"`
	ClientKey  string `json:"clientKey"`
	ServerCA   string `json:"serverCA"`
	ServerName string `json:"serverName"`
}

func (obj TLSPEM) GetTLSConfig() (*tls.Config, error) {
	rootCertPool := x509.NewCertPool()
	if ok := rootCertPool.AppendCertsFromPEM([]byte(obj.ServerCA)); ok == false {
		return nil, fmt.Errorf("Failed to append PEM")
	}

	certs, err := tls.X509KeyPair([]byte(obj.ClientCert), []byte(obj.ClientKey))
	if err != nil {
		return nil, err
	}

	return &tls.Config{
		RootCAs:      rootCertPool,
		Certificates: []tls.Certificate{certs},
		ServerName:   obj.ServerName,
	}, nil
}
