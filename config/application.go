package cfg

// aka information ? with configuration and application
// aka application ? with configuration and information

import (
	"fmt"
	"time"
)

// __________________ Application __________________

var (
	name      string
	version   string
	gitHash   string
	buildDate string
)

type Application struct {
	Name    string    `json:"name"`
	Version string    `json:"version"`
	Build   buildInfo `json:"build"`
}

type buildInfo struct {
	Number string `json:"number"`
	Date   string `json:"date"` // ISO8601 UTC ?
}

func (a Application) String() string {
	return fmt.Sprintf("%v %v (%v %v)", a.Name, a.Version, a.Build.Number, a.Build.Date)
}

func GetApplication() Application {
	return Application{
		Name:    name,
		Version: version,
		Build: buildInfo{
			Number: gitHash,
			Date:   buildDate,
		},
	}
}

// __________________ Instance __________________

var dateStarted int64

func init() {
	dateStarted = time.Now().UTC().Unix()
}

type Instance struct {
	Started int64  `json:"started"`
	UpTime  string `json:"upTime"`
}

func GetInstance() Instance {
	return Instance{
		Started: dateStarted,
		UpTime:  time.Duration(time.Now().UTC().Unix() - dateStarted).String(),
	}
}
