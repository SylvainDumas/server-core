package cfg

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
)

func ReadAndUsage(config interface{}, parsers ...Parser) error {
	// Create configuration reader with ordered reader
	var reader = NewReader().AddParser(parsers...)

	// Read config, and on error display usage
	if err := reader.Read(config); err != nil {
		fmt.Println(GetApplication().String())
		reader.Usage()
		return err
	}

	return nil
}

// __________________________ Reader __________________________

var ErrNotFound = errors.New("")

func NewReader() *Reader {
	return &Reader{}
}

type Parser interface {
	Read(interface{}) error
	Usage()
}

type Reader struct {
	parsers []Parser
}

func (obj *Reader) AddParser(values ...Parser) *Reader {
	for _, v := range values {
		if v != nil {
			obj.parsers = append(obj.parsers, v)
		}
	}
	return obj
}

func (obj *Reader) Read(config interface{}) error {
	// Use each parser to load configuration
	for _, v := range obj.parsers {
		if err := v.Read(config); err == nil {
			return err
		} else if err != ErrNotFound {
			return err
		}
	}

	return errors.New("No configuration found")
}

func (obj *Reader) Usage() {
	for _, v := range obj.parsers {
		v.Usage()
	}
}

// __________________________ Return default __________________________

type ReturnDefault struct {
}

func (obj *ReturnDefault) Read(config interface{}) error {
	return nil
}

func (obj *ReturnDefault) Usage() {
}

// __________________________ Parser File common __________________________

type parserFile struct{}

func (obj *parserFile) readURI(uri string, config interface{}) error {
	// Load configuration from file
	content, err := ioutil.ReadFile(uri)
	if err != nil {
		return err
	}

	// Decode
	return json.Unmarshal(content, config)
}

// __________________________ Parser File Env __________________________

type ParserFileEnv struct {
	parserFile
	Env string
}

func (obj *ParserFileEnv) Read(config interface{}) error {
	// Get configuration file name from env
	var uri = os.Getenv(obj.Env)
	if uri == "" {
		return ErrNotFound
	}

	return obj.readURI(uri, config)
}

func (obj *ParserFileEnv) Usage() {
	fmt.Printf("Specify configuration file by OS environment\n\t%s\n", obj.Env)
}

// __________________________ Parser File Command line __________________________

type ParserFileCmd struct {
	parserFile
	Flag        string
	Description string
	commandLine *flag.FlagSet
}

func (obj *ParserFileCmd) Read(config interface{}) error {
	obj.commandLine = flag.NewFlagSet(os.Args[0], flag.ContinueOnError)

	// Get configuration file name from command line
	var uri string
	obj.commandLine.StringVar(&uri, obj.Flag, "", obj.Description)
	obj.commandLine.Parse(os.Args[1:])
	if uri == "" {
		return ErrNotFound
	}

	return obj.readURI(uri, config)
}

func (obj *ParserFileCmd) Usage() {
	fmt.Printf("Specify configuration file by command line")
	if obj.commandLine != nil {
		obj.commandLine.PrintDefaults()
	}
}
