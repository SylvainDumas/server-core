package errs

import (
	"fmt"
	"strings"
)

type Err struct {
	Err  error                  `json:"error"`
	Meta map[string]interface{} `json:"meta,omitempty"`
}

func (obj *Err) Error() string {
	var ret = make([]string, 0, len(obj.Meta)+1)
	ret = append(ret, obj.Err.Error())
	for k, v := range obj.Meta {
		ret = append(ret, fmt.Sprintf("%v=%v", k, v))
	}

	return strings.Join(ret, " ")
}
