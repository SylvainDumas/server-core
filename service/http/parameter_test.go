package httpsvc

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	svc "gitlab.com/SylvainDumas/server-core/service/common"
)

func Test_Parameter_NilObject(t *testing.T) {
	var nilParameter = Parameter(nil, nil)
	if nilParameter != nil {
		t.Error("Parameter builder must return a nil pointer")
	}

	if err := nilParameter.Error(); err != svc.ErrNilPointerObject {
		t.Error("Error detecting nil pointer error object")
	}

	if err := nilParameter.Route("route", nil).Error(); err != svc.ErrNilPointerObject {
		t.Error("Error detecting nil pointer error object")
	}

	if err := nilParameter.Optional("queryParam", nil).Error(); err != svc.ErrNilPointerObject {
		t.Error("Error detecting nil pointer error object")
	}

	if err := nilParameter.Required("queryParam", nil).Error(); err != svc.ErrNilPointerObject {
		t.Error("Error detecting nil pointer error object")
	}

	if err := nilParameter.Query("queryParam", nil, false).Error(); err != svc.ErrNilPointerObject {
		t.Error("Error detecting nil pointer error object")
	}

	if err := nilParameter.Body(nil).Error(); err != svc.ErrNilPointerObject {
		t.Error("Error detecting nil pointer error object")
	}
}

func Test_Parameter_Route(t *testing.T) {
	request, _ := http.NewRequest("GET", "http://example.com/id/1?foo=bar", nil)

	// Check no filtered route parameters
	err := Parameter(request, nil).Route("toto", nil).Error()
	if err == nil || strings.Contains(err.Error(), "not found in path") == false {
		t.Errorf("error no route parameters defined expected, get %v", err)
	}

	// Check not found in route parameters
	err = Parameter(request, map[string]string{"id": "1"}).Route("toto", nil).Error()
	if err == nil || strings.Contains(err.Error(), "not found in path") == false {
		t.Errorf("error route parameter not found expected, get %v", err)
	}

	// Check found in route parameters
	var valueString string
	if err = Parameter(request, map[string]string{"id": "1"}).Route("id", &valueString).Error(); err != nil || valueString != "1" {
		t.Error(err)
	}
	var valueInt int
	if err = Parameter(request, map[string]string{"id": "1"}).Route("id", &valueInt).Error(); err != nil || valueInt != 1 {
		t.Error(err)
	}
}

func Test_Required(t *testing.T) {
	req, _ := http.NewRequest("GET", "http://example.com?foo=bar&fee=-1&boz=6&boz=18", nil)
	// Check missing
	var missing string
	if err := Parameter(req, nil).Required("test", &missing).Error(); err == nil {
		t.Error("required missing query failed")
	}

	// Check present
	var foo string
	var expectedFoo = "bar"
	if err := Parameter(req, nil).Required("foo", &foo).Error(); err != nil {
		t.Error("required existing query failed")
	} else if foo != expectedFoo {
		t.Errorf("expected value %v get value %v", expectedFoo, foo)
	}
	// Check present and convert
	var fee int
	if err := Parameter(req, nil).Required("fee", &fee).Error(); err != nil {
		t.Error("required existing query and convert failed")
	}

	// Check present
	var bozString []string
	if err := Parameter(req, nil).Required("boz", &bozString).Error(); err != nil {
		t.Error("required existing query failed")
	}
	var bozInteger []int
	if err := Parameter(req, nil).Required("boz", &bozInteger).Error(); err != nil {
		t.Error("required existing query failed")
	}
}

func Test_Optional(t *testing.T) {
	req, _ := http.NewRequest("GET", "http://example.com?foo=bar&fee=-1&boz=6&boz=18", nil)
	// Check missing and no value change
	var expectedMissing = "missing"
	var missing string = expectedMissing
	if err := Parameter(req, nil).Optional("test", &missing).Error(); err != nil {
		t.Error(err)
	} else if missing != expectedMissing {
		t.Errorf("expected value %v get value %v", expectedMissing, missing)
	}

	// Check present
	var foo string
	var expectedFoo = "bar"
	if err := Parameter(req, nil).Optional("foo", &foo).Error(); err != nil {
		t.Error("required existing query failed")
	} else if foo != expectedFoo {
		t.Errorf("expected value %v get value %v", expectedFoo, foo)
	}
	// Check present and convert
	var fee int
	if err := Parameter(req, nil).Optional("fee", &fee).Error(); err != nil {
		t.Error("required existing query and convert failed")
	}

	// Check present
	var bozString []string
	if err := Parameter(req, nil).Optional("boz", &bozString).Error(); err != nil {
		t.Error("required existing query failed")
	}
	var bozInteger []int
	if err := Parameter(req, nil).Optional("boz", &bozInteger).Error(); err != nil {
		t.Error("required existing query failed")
	}
}

func Test_Body(t *testing.T) {
	req, _ := http.NewRequest("GET", "http://example.com?foo=bar&fee=-1&boz=6&boz=18", nil)
	var exceptedBytes = []byte("test body")
	req.Body = ioutil.NopCloser(bytes.NewReader(exceptedBytes))

	var body []byte
	if err := Parameter(req, nil).Body(&body).Error(); err != nil {
		t.Error(err)
	} else if bytes.Equal(exceptedBytes, body) == false {
		t.Errorf(`expected: %s - get: %s`, exceptedBytes, body)
	}

	// All Content Type tests and decoding are made in decoder_test.go
}
