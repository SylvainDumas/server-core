package httpsvc

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"gitlab.com/SylvainDumas/server-core/internal"
	"gitlab.com/SylvainDumas/server-core/net/mediatype"
	svc "gitlab.com/SylvainDumas/server-core/service/common"
)

// Parameter take request and route key value to generate an object to easily parse and extract values
func Parameter(request *http.Request, routeKeyValue map[string]string) *parameter {
	if request == nil {
		return nil
	}

	return &parameter{
		request:       request,
		routeKeyValue: routeKeyValue,
		err:           request.ParseForm(),
	}
}

type parameter struct {
	request       *http.Request
	routeKeyValue map[string]string
	err           error
}

// Error return the first error encountered by the chained function call
func (obj *parameter) Error() error {
	if obj == nil {
		return svc.ErrNilPointerObject
	}
	return obj.err
}

// Route give and convert to dst destination, route parameters named name (part of the path URL)
func (obj *parameter) Route(name string, dst interface{}) *parameter {
	if obj == nil || obj.Error() != nil {
		return obj
	}

	value, found := obj.routeKeyValue[name]
	if found == false {
		obj.err = fmt.Errorf("Key %v not found in path", name)
	} else {
		obj.err = internal.DecodeSingleParameterValue(value, dst)
	}

	return obj
}

// Required give and convert to dst destination, query parameters named name (part of the query URL).
// If the query parameter is not found, an error is set.
func (obj *parameter) Required(name string, dst interface{}) *parameter {
	return obj.Query(name, dst, true)
}

// Optional give and convert to dst destination, query parameters named name (part of the query URL).
// If the query parameter is not found, dst value is not modified
func (obj *parameter) Optional(name string, dst interface{}) *parameter {
	return obj.Query(name, dst, false)
}

func (obj *parameter) Query(name string, dst interface{}, required bool) *parameter {
	if obj == nil || obj.Error() != nil {
		return obj
	}

	values, found := obj.request.Form[name]
	if found {
		obj.err = internal.DecodeQueryParameterValue(values, dst)
	} else if required {
		obj.err = fmt.Errorf("Query parameter %v not found", name)
	}

	return obj
}

func (obj *parameter) Body(layout interface{}) *parameter {
	if obj == nil || obj.Error() != nil {
		return obj
	}

	defer obj.request.Body.Close()
	obj.err = decodeBody(obj.request.Header, obj.request.Body, layout)

	return obj
}

func decodeBody(headers http.Header, body io.ReadCloser, out interface{}) (err error) {
	switch typed := out.(type) {
	case nil:
		_, err = io.Copy(ioutil.Discard, body)
	case *[]byte:
		*typed, err = ioutil.ReadAll(body)
	default:
		// Get Content-Type and decode following it
		contentType := headers.Get(mediatype.ContentTypeHeader)
		return mediatype.DecodeReader(contentType, body, out)
	}

	return
}
