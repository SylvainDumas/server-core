package ginsvc

import (
	"errors"
	"fmt"
	"net/http"
	"reflect"

	svc "gitlab.com/SylvainDumas/server-core/service/common"

	"github.com/gin-gonic/gin"
)

func Create(service Group) (http.Handler, error) {
	return CreateWithAccessor(service, svc.Middlewares)
}

func CreateWithAccessor(service Group, middelwaresAccessor svc.Accessor) (http.Handler, error) {
	// Service creation
	var handler = gin.Default()

	var router = handler.Group(service.Pattern)

	// Add middlewares to service level
	middlewares, err := getMiddlewares(middelwaresAccessor, service.Middlewares...)
	if err != nil {
		return nil, err
	}
	router.Use(middlewares...)

	if err := FillGroup(router, service.Routes, middelwaresAccessor); err != nil {
		return nil, err
	}

	return handler, nil
}

func FillGroup(group *gin.RouterGroup, routes Routes, middelwaresAccessor svc.Accessor) error {
	for _, v := range routes {
		switch value := reflect.ValueOf(v).Interface().(type) {
		case Route:
			middlewares, err := getMiddlewares(middelwaresAccessor, value.Middlewares...)
			if err != nil {
				return err
			}
			group.Handle(value.Method, value.Pattern, append(middlewares, value.Handler)...)
		case Group:
			middlewares, err := getMiddlewares(middelwaresAccessor, value.Middlewares...)
			if err != nil {
				return err
			}

			if err := FillGroup(group.Group(value.Pattern, middlewares...), value.Routes, middelwaresAccessor); err != nil {
				return err
			}
		default:
			return fmt.Errorf("Unknown path type")
		}
	}

	return nil
}

func getMiddlewares(accessor svc.Accessor, name ...string) ([]gin.HandlerFunc, error) {
	if accessor == nil {
		accessor = svc.Middlewares
	}

	var ret []gin.HandlerFunc
	for _, v := range name {
		middle, ok := accessor.Get(v).(gin.HandlerFunc)
		if ok == false {
			return nil, errors.New("error in getting middleware")
		}
		ret = append(ret, middle)
	}

	return ret, nil
}
