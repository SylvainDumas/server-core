package ginsvc

import "github.com/gin-gonic/gin"

type Routes []interface{}

type Group struct {
	Pattern     string
	Middlewares []string
	Routes      Routes
}

type Route struct {
	Name        string
	Method      string
	Pattern     string
	Middlewares []string
	Handler     func(c *gin.Context)
}
