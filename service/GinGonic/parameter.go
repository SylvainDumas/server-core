package ginsvc

import (
	"fmt"
	"io"
	"io/ioutil"

	"github.com/gin-gonic/gin"

	"gitlab.com/SylvainDumas/server-core/internal"
	svc "gitlab.com/SylvainDumas/server-core/service/common"
)

// Parameter take request and route key value to generate an object to easily parse and extract values
func Parameter(ctx *gin.Context) *parameter {
	if ctx == nil {
		return nil
	}

	return &parameter{ctx: ctx, err: ctx.Request.ParseForm()}
}

type parameter struct {
	ctx *gin.Context
	err error
}

// Error return the first error encountered by the chained function call
func (obj *parameter) Error() error {
	if obj == nil {
		return svc.ErrNilPointerObject
	}
	return obj.err
}

// Route give and convert to dst destination, route parameters named name (part of the path URL)
func (obj *parameter) Route(name string, dst interface{}) *parameter {
	if obj == nil || obj.Error() != nil {
		return obj
	}

	value, found := obj.ctx.Params.Get(name)
	if found == false {
		obj.err = fmt.Errorf("Key %v not found in path", name)
	} else {
		obj.err = internal.DecodeSingleParameterValue(value, dst)
	}

	return obj
}

// Required give and convert to dst destination, query parameters named name (part of the query URL).
// If the query parameter is not found, an error is set.
func (obj *parameter) Required(name string, dst interface{}) *parameter {
	return obj.Query(name, dst, true)
}

// Optional give and convert to dst destination, query parameters named name (part of the query URL).
// If the query parameter is not found, dst value is not modified
func (obj *parameter) Optional(name string, dst interface{}) *parameter {
	return obj.Query(name, dst, false)
}

// Query give and convert all query parameters, required or not, from the query and the POST or PUT form data.
func (obj *parameter) Query(name string, dst interface{}, required bool) *parameter {
	if obj == nil || obj.Error() != nil {
		return obj
	}

	values, found := obj.ctx.Request.Form[name]
	if found {
		obj.err = internal.DecodeQueryParameterValue(values, dst)
	} else if required {
		obj.err = fmt.Errorf("Query parameter %v not found", name)
	}

	return obj
}

// Body return the decoded body content of the request
func (obj *parameter) Body(layout interface{}) *parameter {
	if obj == nil || obj.Error() != nil {
		return obj
	}

	var decodeBody = func() (err error) {
		switch typed := layout.(type) {
		case nil:
			defer obj.ctx.Request.Body.Close()
			_, err = io.Copy(ioutil.Discard, obj.ctx.Request.Body)
		case *[]byte:
			defer obj.ctx.Request.Body.Close()
			*typed, err = ioutil.ReadAll(obj.ctx.Request.Body)
		default:
			return obj.ctx.ShouldBind(layout)
		}

		return
	}

	obj.err = decodeBody()
	return obj
}
