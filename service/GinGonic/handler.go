package ginsvc

import (
	"log"
	"net/http"

	svc "gitlab.com/SylvainDumas/server-core/service/common"

	"github.com/gin-gonic/gin"
)

type GinErrorFn func(c *gin.Context) error

func GinErrorFnHandler(functor GinErrorFn) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Recover PANIC error ?
		// Mode TRACE timer function start
		err := functor(c)
		// Mode TRACE timer function end
		if err == nil {
			return
		}

		switch errType := err.(type) {
		case *svc.ErrBadRequest:
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": errType.Error()})
			log.Println(errType.Error())
		case *svc.ErrNotFound:
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"error": errType.Error()})
			log.Println(errType.Error())
		case *svc.ErrHTTP:
			c.AbortWithStatusJSON(errType.StatusCode, gin.H{"error": errType.Error()})
			log.Println(errType.Error())
		case error:
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": errType.Error()})
			log.Println(errType.Error())
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": http.StatusText(http.StatusInternalServerError)})
			log.Println("unknown error")
		}
	}
}
