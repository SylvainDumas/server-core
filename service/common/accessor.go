package svc

import "sync"

var Models = &accessorImpl{}
var Middlewares = &accessorImpl{}

// __________________________ interface __________________________

type Accessor interface {
	Register(string, interface{})
	Get(string) interface{}
}

// __________________________ implementation __________________________

type accessorImpl struct {
	objects sync.Map
}

func (s *accessorImpl) Get(name string) interface{} {
	object, _ := s.objects.Load(name)
	return object
}

func (s *accessorImpl) Register(name string, object interface{}) {
	if object != nil {
		s.objects.Store(name, object)
	}
}
