package svc

import (
	"errors"

	errs "gitlab.com/SylvainDumas/server-core/error"
)

const nilPointerErrorObject = "nil pointer object"

var ErrNilPointerObject = errors.New(nilPointerErrorObject)

type ErrBadRequest struct {
	errs.Err
}

type ErrNotFound struct {
	errs.Err
}

type ErrHTTP struct {
	errs.Err
	StatusCode int
}
