[![pipeline status](https://gitlab.com/SylvainDumas/server-core/badges/master/pipeline.svg)](https://gitlab.com/SylvainDumas/server-core/-/commits/master)
[![coverage report](https://gitlab.com/SylvainDumas/server-core/badges/master/coverage.svg)](https://gitlab.com/SylvainDumas/server-core/-/commits/master)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

# Passing values at build time for application informations
```shell
go build -ldflags "-X 'gitlab.com/SylvainDumas/server-core/config.build.date=`date -Iseconds`' -X 'gitlab.com/SylvainDumas/server-core/config.build.number=`git rev-parse HEAD`' -X 'gitlab.com/SylvainDumas/server-core/config.name=$APP_NAME' -X 'gitlab.com/SylvainDumas/server-core/config.version=$APP_VERSION'"
```
