package mediatype

import (
	"fmt"
	"mime"
	"strings"
)

// TODO for String methods, no check is done to respect
// -> Types, subtypes, and parameter names are case-insensitive. Parameter values are usually case-sensitive
// Protect this with setter methods and private variables, or in the String methods at choice

// A media type consists of a type and a subtype, which is further structured into a tree. A media type can optionally define a suffix and parameters:
// type "/" [tree "."] subtype ["+" suffix] *[";" parameter]

// Doc:
// # Media type
// - https://en.wikipedia.org/wiki/Media_type
// - https://tools.ietf.org/html/rfc2045
// - https://tools.ietf.org/html/rfc2046
// - https://tools.ietf.org/html/rfc1521
// # Content type
// - https://www.w3.org/Protocols/rfc1341/4_Content-Type.html
// - https://tools.ietf.org/html/rfc6838

// MediaType is defined by RFC2045 and his extensions
// DataType and Format are mandatory
// DataType, Format, and Parameters names are case-insensitive. Parameters values are usually case-sensitive
// DataType regitered: "text" / "image" / "audio" / "video" / "application" / "message" / "multipart" / "x-*"
type MediaType struct {
	DataType   string
	Format     Format
	Parameters map[string]string
}

func (obj MediaType) String() string {
	var ret = fmt.Sprintf("%s/%s", obj.DataType, obj.Format.String())
	return mime.FormatMediaType(ret, obj.Parameters)
}

type Format struct {
	// Tree: empty for standard, vnd for Vendor, prs for Personal or Vanity, x for Unregistered
	Tree    string
	SubType string
	// Suffix: xml, json, ber, der, fastinfoset, wbxml, zip, gzip, cbor, json-seq, and cbor-seq
	Suffix string
}

func (obj Format) String() string {
	var ret = obj.Tree
	if ret != "" {
		ret += "."
	}
	ret += obj.SubType
	if obj.Suffix != "" {
		ret += "+" + obj.Suffix
	}
	return ret
}

func Decode(value string) (MediaType, error) {
	data, params, err := mime.ParseMediaType(value)
	if err != nil {
		return MediaType{}, err
	}

	var decodeFormat = func(value string) (fmt Format) {
		// Extract tree if present
		if idx := strings.Index(value, "."); idx != -1 {
			fmt.Tree = value[:idx]
			value = value[idx+1:]
		}
		// extract subtype and suffix
		if idx := strings.Index(value, "+"); idx != -1 {
			fmt.SubType = value[:idx]
			fmt.Suffix = value[idx+1:]
		} else {
			fmt.SubType = value
		}
		return
	}

	var ret = MediaType{Parameters: params}
	if slash := strings.Index(data, "/"); slash != -1 {
		ret.DataType = data[:slash]
		ret.Format = decodeFormat(data[slash+1:])
	} else {
		ret.DataType = data
	}

	return ret, nil
}
