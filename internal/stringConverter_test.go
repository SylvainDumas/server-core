package internal

import (
	"reflect"
	"testing"
)

func Test_convertToValue(t *testing.T) {
	if err := convertToValue("foo", reflect.Value{}); err == nil {
		t.Error("Expected error for invalid reflect value")
	}
}

func Test_DecodeSingleParameterValue(t *testing.T) {
	// Check nil
	if err := DecodeSingleParameterValue("128", nil); err == nil {
		t.Error("Expected error for nil")
	}
	// Check dst nil pointer
	var dstNil *string
	if err := DecodeSingleParameterValue("128", &dstNil); err == nil {
		t.Error("Expected error for destination nil")
	}
	// Check dst interface
	var dstInterface interface{}
	if err := DecodeSingleParameterValue("128", &dstInterface); err == nil {
		t.Error("Expected error for destination interface")
	}
	// Check dst not a pointer
	var dstNotAPointer string
	if err := DecodeSingleParameterValue("128", dstNotAPointer); err == nil {
		t.Error("Expected error for destination not a pointer")
	}
	// Check dst not a slice
	var dstSlice []string
	if err := DecodeSingleParameterValue("128", dstSlice); err == nil {
		t.Error("Expected error for destination slice")
	}

	// Check string
	var dstString string
	if err := DecodeSingleParameterValue("foo;bar", &dstString); err != nil {
		t.Error(err)
	} else if dstString != "foo;bar" {
		t.Errorf("Expected value foo;bar get value %v", dstString)
	}

	// Check int8 cases
	var int8Max int8
	if err := DecodeSingleParameterValue("127", &int8Max); err != nil {
		t.Error(err)
	} else if int8Max != 127 {
		t.Errorf("Expected value 127 get value %v", int8Max)
	}
	var int8Overflow int8
	if err := DecodeSingleParameterValue("128", &int8Overflow); err == nil {
		t.Error("Expected error for int8 overflow")
	}

	// Check int16 cases
	var int16Max int16
	if err := DecodeSingleParameterValue("32767", &int16Max); err != nil {
		t.Error(err)
	} else if int16Max != 32767 {
		t.Errorf("Expected value 32767 get value %v", int16Max)
	}
	var int16Overflow int16
	if err := DecodeSingleParameterValue("32768", &int16Overflow); err == nil {
		t.Error("Expected error for int16 overflow")
	}

	// Check int32 cases
	var int32Max int32
	if err := DecodeSingleParameterValue("2147483647", &int32Max); err != nil {
		t.Error(err)
	} else if int32Max != 2147483647 {
		t.Errorf("Expected value 2147483647 get value %v", int32Max)
	}
	var int32Overflow int32
	if err := DecodeSingleParameterValue("2147483648", &int32Overflow); err == nil {
		t.Error("Expected error for int32 overflow")
	}

	// Check int64 cases
	var int64Max int64
	if err := DecodeSingleParameterValue("9223372036854775807", &int64Max); err != nil {
		t.Error(err)
	} else if int64Max != 9223372036854775807 {
		t.Errorf("Expected value 9223372036854775807 get value %v", int64Max)
	}
	var int64Overflow int64
	if err := DecodeSingleParameterValue("9223372036854775808", &int64Overflow); err == nil {
		t.Error("Expected error for int64 overflow")
	}

	// Check uint8 cases
	var uint8Max uint8
	if err := DecodeSingleParameterValue("255", &uint8Max); err != nil {
		t.Error(err)
	} else if uint8Max != 255 {
		t.Errorf("Expected value 255 get value %v", uint8Max)
	}
	var uint8Overflow uint8
	if err := DecodeSingleParameterValue("256", &uint8Overflow); err == nil {
		t.Error("Expected error for uint8 overflow")
	}

	// Check uint16 cases
	var uint16Max uint16
	if err := DecodeSingleParameterValue("65535", &uint16Max); err != nil {
		t.Error(err)
	} else if uint16Max != 65535 {
		t.Errorf("Expected value 65535 get value %v", uint16Max)
	}
	var uint16Overflow uint16
	if err := DecodeSingleParameterValue("65536", &uint16Overflow); err == nil {
		t.Error("Expected error for uint16 overflow")
	}

	// Check uint32 cases
	var uint32Max uint32
	if err := DecodeSingleParameterValue("4294967295", &uint32Max); err != nil {
		t.Error(err)
	} else if uint32Max != 4294967295 {
		t.Errorf("Expected value 4294967295 get value %v", uint32Max)
	}
	var uint32Overflow uint32
	if err := DecodeSingleParameterValue("4294967296", &uint32Overflow); err == nil {
		t.Error("Expected error for uint32 overflow")
	}

	// Check uint64 cases
	var uint64Max uint64
	if err := DecodeSingleParameterValue("18446744073709551615", &uint64Max); err != nil {
		t.Error(err)
	} else if uint64Max != 18446744073709551615 {
		t.Errorf("Expected value 18446744073709551615 get value %v", uint64Max)
	}
	var uint64Overflow uint64
	if err := DecodeSingleParameterValue("18446744073709551616", &uint64Overflow); err == nil {
		t.Error("Expected error for uint64 overflow")
	}

	// Check float32 cases
	var float32Max float32
	if err := DecodeSingleParameterValue("3.40282346638528859811704183484516925440e+38", &float32Max); err != nil {
		t.Error(err)
	} else if float32Max != 3.40282346638528859811704183484516925440e+38 {
		t.Errorf("Expected value 3.40282346638528859811704183484516925440e+38 get value %v", float32Max)
	}
	var float32Overflow float32
	if err := DecodeSingleParameterValue("3.40282346638528859811704183484516925440e+39", &float32Overflow); err == nil {
		t.Error("Expected error for float32 overflow")
	}

	// Check float64 cases
	var float64Max float64
	if err := DecodeSingleParameterValue("1.797693134862315708145274237317043567981e+308", &float64Max); err != nil {
		t.Error(err)
	} else if float64Max != 1.797693134862315708145274237317043567981e+308 {
		t.Errorf("Expected value 1.797693134862315708145274237317043567981e+308 get value %v", float64Max)
	}
	var float64Overflow float64
	if err := DecodeSingleParameterValue("1.797693134862315708145274237317043567981e+309", &float64Overflow); err == nil {
		t.Error("Expected error for uint64 overflow")
	}

	// Check bool cases
	var bValue bool
	if err := DecodeSingleParameterValue("true", &bValue); err != nil {
		t.Error(err)
	} else if bValue != true {
		t.Errorf("Expected value true get value %v", bValue)
	}
	if err := DecodeSingleParameterValue("false", &bValue); err != nil {
		t.Error(err)
	} else if bValue != false {
		t.Errorf("Expected value false get value %v", bValue)
	}
	if err := DecodeSingleParameterValue("", &bValue); err == nil {
		t.Error("Expected error for bool")
	}
}

func Test_decodeMultiParameterValue(t *testing.T) {
	var arrayTest1 = []string{"foo", "bar"}
	// Check nil
	if err := decodeMultiParameterValue(arrayTest1, nil); err == nil {
		t.Error("Expected error for nil")
	}
	// Check dst nil pointer
	var dstNil *[]string
	if err := decodeMultiParameterValue(arrayTest1, &dstNil); err == nil {
		t.Error("Expected error for destination nil")
	}
	// Check dst interface
	var dstInterface interface{}
	if err := decodeMultiParameterValue(arrayTest1, &dstInterface); err == nil {
		t.Error("Expected error for destination interface")
	}
	// Check dst not a pointer
	var dstNotAPointer string
	if err := decodeMultiParameterValue(arrayTest1, dstNotAPointer); err == nil {
		t.Error("Expected error for destination not a pointer")
	}
	// Check dst not a slice
	var dstSingle string
	if err := decodeMultiParameterValue(arrayTest1, dstSingle); err == nil {
		t.Error("Expected error for destination slice")
	}

	// Check []string
	var dstString []string
	if err := decodeMultiParameterValue(arrayTest1, &dstString); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(dstString, arrayTest1) == false {
		t.Errorf("Expected value foo;bar get value %v", dstString)
	}

	// Check int8 cases
	var int8Max []int8
	var expectedInt8Max = []int8{68, 127}
	if err := decodeMultiParameterValue([]string{"68", "127"}, &int8Max); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(int8Max, expectedInt8Max) == false {
		t.Errorf("Expected value %v get value %v", expectedInt8Max, int8Max)
	}
	var int8Overflow []int8
	if err := decodeMultiParameterValue([]string{"68", "128"}, &int8Overflow); err == nil {
		t.Error("Expected error for []int8 overflow")
	}

	// Check int16 cases
	var int16Max []int16
	var expectedInt16Max = []int16{68, 32767}
	if err := decodeMultiParameterValue([]string{"68", "32767"}, &int16Max); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(int16Max, expectedInt16Max) == false {
		t.Errorf("Expected value %v get value %v", expectedInt16Max, int16Max)
	}
	var int16Overflow []int16
	if err := decodeMultiParameterValue([]string{"68", "32768"}, &int16Overflow); err == nil {
		t.Error("Expected error for []int16 overflow")
	}

	// Check int32 cases
	var int32Max []int32
	var expectedInt32Max = []int32{68, 2147483647}
	if err := decodeMultiParameterValue([]string{"68", "2147483647"}, &int32Max); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(int32Max, expectedInt32Max) == false {
		t.Errorf("Expected value %v get value %v", expectedInt32Max, int32Max)
	}
	var int32Overflow []int32
	if err := decodeMultiParameterValue([]string{"68", "2147483648"}, &int32Overflow); err == nil {
		t.Error("Expected error for []int32 overflow")
	}

	// Check int64 cases
	var int64Max []int64
	var expectedInt64Max = []int64{68, 9223372036854775807}
	if err := decodeMultiParameterValue([]string{"68", "9223372036854775807"}, &int64Max); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(int64Max, expectedInt64Max) == false {
		t.Errorf("Expected value %v get value %v", expectedInt64Max, int64Max)
	}
	var int64Overflow []int64
	if err := decodeMultiParameterValue([]string{"68", "9223372036854775808"}, &int64Overflow); err == nil {
		t.Error("Expected error for []int64 overflow")
	}

	// Check uint8 cases
	var uint8Max []uint8
	var expectedUint8Max = []uint8{68, 255}
	if err := decodeMultiParameterValue([]string{"68", "255"}, &uint8Max); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(uint8Max, expectedUint8Max) == false {
		t.Errorf("Expected value %v get value %v", expectedUint8Max, uint8Max)
	}
	var uint8Overflow []uint8
	if err := decodeMultiParameterValue([]string{"68", "256"}, &uint8Overflow); err == nil {
		t.Error("Expected error for []uint8 overflow")
	}

	// Check uint16 cases
	var uint16Max []uint16
	var expectedUint16Max = []uint16{68, 65535}
	if err := decodeMultiParameterValue([]string{"68", "65535"}, &uint16Max); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(uint16Max, expectedUint16Max) == false {
		t.Errorf("Expected value %v get value %v", expectedUint16Max, uint16Max)
	}
	var uint16Overflow []uint16
	if err := decodeMultiParameterValue([]string{"68", "65536"}, &uint16Overflow); err == nil {
		t.Error("Expected error for []uint16 overflow")
	}

	// Check uint32 cases
	var uint32Max []uint32
	var expectedUint32Max = []uint32{68, 4294967295}
	if err := decodeMultiParameterValue([]string{"68", "4294967295"}, &uint32Max); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(uint32Max, expectedUint32Max) == false {
		t.Errorf("Expected value %v get value %v", expectedUint32Max, uint32Max)
	}
	var uint32Overflow []uint32
	if err := decodeMultiParameterValue([]string{"68", "4294967296"}, &uint32Overflow); err == nil {
		t.Error("Expected error for []uint32 overflow")
	}

	// Check uint64 cases
	var uint64Max []uint64
	var expectedUint64Max = []uint64{68, 18446744073709551615}
	if err := decodeMultiParameterValue([]string{"68", "18446744073709551615"}, &uint64Max); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(uint64Max, expectedUint64Max) == false {
		t.Errorf("Expected value %v get value %v", expectedUint64Max, uint64Max)
	}
	var uint64Overflow []uint64
	if err := decodeMultiParameterValue([]string{"68", "18446744073709551616"}, &uint64Overflow); err == nil {
		t.Error("Expected error for []uint64 overflow")
	}

	// Check float32 cases
	var float32Max []float32
	var expectedFloat32Max = []float32{68.5, 3.40282346638528859811704183484516925440e+38}
	if err := decodeMultiParameterValue([]string{"68.5", "3.40282346638528859811704183484516925440e+38"}, &float32Max); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(float32Max, expectedFloat32Max) == false {
		t.Errorf("Expected value %v get value %v", expectedFloat32Max, float32Max)
	}
	var float32Overflow []float32
	if err := decodeMultiParameterValue([]string{"68", "3.40282346638528859811704183484516925440e+39"}, &float32Overflow); err == nil {
		t.Error("Expected error for []float32 overflow")
	}

	// Check float64 cases
	var float64Max []float64
	var expectedFloat64Max = []float64{68, 1.797693134862315708145274237317043567981e+308}
	if err := decodeMultiParameterValue([]string{"68", "1.797693134862315708145274237317043567981e+308"}, &float64Max); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(float64Max, expectedFloat64Max) == false {
		t.Errorf("Expected value %v get value %v", expectedFloat64Max, float64Max)
	}
	var float64Overflow []float64
	if err := decodeMultiParameterValue([]string{"68", "1.797693134862315708145274237317043567981e+309"}, &float64Overflow); err == nil {
		t.Error("Expected error for []float64 overflow")
	}

	// Check bool cases
	var bools []bool
	var expectedbools = []bool{true, false, true}
	if err := decodeMultiParameterValue([]string{"True", "false", "1"}, &bools); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(bools, expectedbools) == false {
		t.Errorf("Expected value %v get value %v", expectedbools, bools)
	}
	var badBool []bool
	if err := decodeMultiParameterValue([]string{"TrUe", "FaLsE"}, &badBool); err == nil {
		t.Error("Expected error for []bool")
	}

}

func Test_decodeQueryParameterValue(t *testing.T) {
	var dstString string
	if err := DecodeQueryParameterValue([]string{}, &dstString); err == nil {
		t.Error("Expected error for no data")
	}

	var expectedString = "foo"
	var expectedSlice = []string{"foo", "bar"}

	var oneString string
	if err := DecodeQueryParameterValue([]string{expectedString}, &oneString); err != nil {
		t.Error(err)
	} else if oneString != expectedString {
		t.Errorf("Expected value %v get value %v", expectedString, oneString)
	}
	if err := DecodeQueryParameterValue(expectedSlice, &oneString); err == nil {
		t.Error("Expected error array N to 1 value")
	}

	var sliceString []string
	if err := DecodeQueryParameterValue([]string{expectedString}, &sliceString); err == nil {
		t.Error("Expected error array 1 to N value")
	}
	if err := DecodeQueryParameterValue(expectedSlice, &sliceString); err != nil {
		t.Error(err)
	} else if reflect.DeepEqual(expectedSlice, sliceString) == false {
		t.Errorf("Expected value %v get value %v", expectedSlice, sliceString)
	}
}
