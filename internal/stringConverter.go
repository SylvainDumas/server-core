package internal

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
)

// ____________________________ convert ____________________________

func convertToValue(value string, rv reflect.Value) error {
	if rv.IsValid() == false {
		return fmt.Errorf("unable to decode value %v in invalid type", value)
	}

	switch rv.Kind() {
	case reflect.String:
		rv.SetString(value)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		n, err := strconv.ParseInt(value, 10, 64)
		if err != nil || rv.OverflowInt(n) {
			return fmt.Errorf("unable to decode value %v in type %v", value, rv.Type().Name())
		}
		rv.SetInt(n)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		n, err := strconv.ParseUint(value, 10, 64)
		if err != nil || rv.OverflowUint(n) {
			return fmt.Errorf("unable to decode value %v in type %v", value, rv.Type().Name())
		}
		rv.SetUint(n)
	case reflect.Float32, reflect.Float64:
		n, err := strconv.ParseFloat(value, rv.Type().Bits())
		if err != nil || rv.OverflowFloat(n) {
			return fmt.Errorf("unable to decode value %v in type %v", value, rv.Type().Name())
		}
		rv.SetFloat(n)
	case reflect.Bool:
		n, err := strconv.ParseBool(value)
		if err != nil {
			return fmt.Errorf("unable to decode value %v in type %v", value, rv.Type().Name())
		}
		rv.SetBool(n)
	default:
		return fmt.Errorf("unable to decode value %v in type %v", value, rv.Type().Name())
	}

	return nil
}

func DecodeSingleParameterValue(value string, dst interface{}) error {
	if dst == nil {
		return fmt.Errorf("unable to decode value %v in type nil", value)
	}
	rv := reflect.ValueOf(dst)
	if rv.Kind() != reflect.Ptr || rv.IsNil() {
		return fmt.Errorf("unable to decode value %v in type %v", value, rv.Type().Name())
	}

	return convertToValue(value, rv.Elem())
}

func decodeMultiParameterValue(values []string, dst interface{}) error {
	if dst == nil {
		return fmt.Errorf("unable to decode value %+v in type nil", values)
	}
	rv := reflect.ValueOf(dst)
	if rv.Kind() != reflect.Ptr || rv.IsNil() {
		return fmt.Errorf("unable to decode value %+v in type %v", values, rv.Type().Name())
	}

	array := rv.Elem()
	if array.Kind() != reflect.Slice {
		return fmt.Errorf("unable to decode value %+v in type %v", values, array.Type().Name())
	}

	length := len(values)
	array.Set(reflect.MakeSlice(array.Type(), length, length))
	for i := 0; i < length; i++ {
		if err := convertToValue(values[i], array.Index(i)); err != nil {
			return err
		}
	}

	return nil
}

func DecodeQueryParameterValue(values []string, dst interface{}) error {
	switch len(values) {
	case 0:
		return errors.New("no data")
	case 1:
		return DecodeSingleParameterValue(values[0], dst)
	default:
		return decodeMultiParameterValue(values, dst)
	}
}
